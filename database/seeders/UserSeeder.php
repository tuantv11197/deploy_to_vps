<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'name' => 'tuantv',
            'email' => 'tuan@gmail.com',
            'password' => bcrypt('123456')
        ];
        User::create($data);
    }
}
